var _ = require('underscore')

/**
 * Creates a new rectangle. The position and dimensions are designed to be
 * easily compatible with an HTML canvas, meaning left and down are considered
 * positive.
 * @param x the x value of the base corner
 * @param y the y value of the base corner
 * @param width
 * @param height
 * @constructor
 */
var Rectangle = function(x, y, width, height) {
	if (!width || !height) throw "A Rectangle must have a non-zero width and height"
	if (!_.isNumber(x) || !_.isNumber(y) || !_.isNumber(width) || !_.isNumber(height)) {
		throw "X, Y, Width, and Height must all be numbers"
	}
	
	this.x = x
	this.y = y
	this.width = width
	this.height = height
}

Rectangle.Point = function(x, y) {
	this.x = x
	this.y = y
}

Rectangle.Point.prototype.equals = function(/*Point*/ that) {
	return this.x === that.x && this.y === that.y
}

Rectangle.Point.prototype.existsIn = function(points) {
	return !!_.findWhere(points, function(p) {
		return this.equals(p)
	}.bind(this))
}

var xor = function(a, b) {
	return (a && !b) || (!a && b)
}

/**
 * Checks if the given point is wholly surrounded by this rectangle
 * @param x1
 * @param y1
 * @returns true if the point is wholly contained
 */
Rectangle.prototype.surroundsPoint = function(x1, y1) {
	return this._surroundsX(x1) && this._surroundsY(y1)
}

Rectangle.prototype._surroundsX = function(x1) {
	return (x1 > this.x && x1 < this.x + this.width)
}

Rectangle.prototype._includesX = function(x1) {
	return (x1 >= this.x && x1 <= this.x + this.width)
}

Rectangle.prototype._surroundsY = function(y1) {
	return (y1 > this.y && y1 < this.y + this.height)
}

Rectangle.prototype._includesY = function(y1) {
	return (y1 >= this.y && y1 < this.y + this.height)
}

/**
 * Checks if the given point is anywhere on the boundary of this rectangle
 * @param x1
 * @param y1
 * @returns true if the point is incident
 */
Rectangle.prototype.isIncident = function(x1, y1) {
	return (this._incidentX(x1) && this._includesY(y1)) || (this._incidentY(y1) && this._includesX(x1))
}

Rectangle.prototype._incidentX = function(x1) {
	return (x1 == this.x || x1 == this.x + this.width) 
}

Rectangle.prototype._incidentY = function(y1) {
	return (y1 == this.y || y1 == this.y + this.height)
}

/**
 * Checks if this is wholly contained within that
 * @param that
 * @returns {*}
 */
Rectangle.prototype.isContainedWithin = function(/*Rectangle*/ that) {
	var corner1Inside = that.surroundsPoint(this.x, this.y)
	var corner2Inside = that.surroundsPoint(this.x + this.width, this.y + this.height)
	
	return corner1Inside && corner2Inside
}

/**
 * Checks for adjacency
 * @param that
 * @returns true if there's any overlap of the borders
 */
Rectangle.prototype.isAdjacentTo = function(/*Rectangle*/ that) {
	var corner1Incident = that.isIncident(this.x, this.y)
	var corner2Incident = that.isIncident(this.x + this.width, this.y + this.height)
	
	var thatCorner1Incident = this.isIncident(that.x, that.y)
	var thatCorner2Incident = this.isIncident(that.x + that.width, that.y + that.height)
	
	return xor(corner1Incident, corner2Incident) || xor(thatCorner1Incident, thatCorner2Incident)
}

/**
 * Finds the points of intersection
 * @param that
 * @return an array of Point, or an empty array if there's no intersection
 */
Rectangle.prototype.findIntersections = function(/*Rectangle*/ that) {
	
	//If any of the corners are on the other rectangles border, there is no intersection
	var corner1Incident = that.isIncident(this.x, this.y)
	var corner2Incident = that.isIncident(this.x + this.width, this.y + this.height)

	var thatCorner1Incident = this.isIncident(that.x, that.y)
	var thatCorner2Incident = this.isIncident(that.x + that.width, that.y + that.height)
	
	if (corner1Incident || corner2Incident || thatCorner1Incident || thatCorner2Incident) {
		return []
	}
	
	var P = Rectangle.Point
	var allPossiblePoints = [
		new P(this.x, that.y),
		new P(this.x, that.y + that.height),
		new P(this.x + this.width, that.y),
		new P(this.x + this.width, that.y + that.height),
		new P(that.x, this.y),
		new P(that.x, this.y + this.height),
		new P(that.x + that.width, this.y),
		new P(that.x + that.width, this.y + this.height)
	]
	
	return _.filter(allPossiblePoints, function(point) {
		return this.isIncident(point.x, point.y) && that.isIncident(point.x, point.y)
	}.bind(this))
}

module.exports = Rectangle
