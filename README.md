# Readme

## Overview

Rectangles provides an npm module in which is defined a Rectangles type, including several methods for analyzing the relationtionship between rectangles. Rectangles defines a rectangle as a starting point: (x, y) and a height and width.

## Getting Started

Rectangles assumes you have a working installation of nodejs and npm. You can install by running `npm install https://samuelbjohnson@bitbucket.org/samuelbjohnson/rectangles.git` and then using `require('rectangles/rectangles')` in your code.

### Using the Rectangle type and Rectangle.Point sub-type

You can use the Rectangle type in your own code by requiring it and then invoking the constructor:

```
#!javascript

var Rectangle = require('rectangles/rectangles')
var rect = new Rectangle(x, y, width, height)
```

after which you can compare it with other rectangle objects.

#### `.isContainedWithin(rect)` 

Returns true if this rectangle is wholly contained within `rect`

#### `.isAdjacentTo(rect)`

Returns true if this rectangle is adjacent to `rect`

#### `.findIntersections(rect)`

Returns an array of Rectangle.Point objects representing the points of intersection between the borders of this rectangle and `rect` or an empty array if there are no intersections.

## Assumptions

A rectangle is assumed to have an infinitesimally thin border (limited by the floating point implementation used by the JavaScript engine running the code). Overlap, for the purposes of adjacency and containment is determined by equality of the x and y values of the corners of the rectangle.

Rectangles are assumed to be adjacent if part of all of exactly one side of each overlap. Therefore a rectangle is not said to be adjacent to itself (as all four sides overlap). A single "point" of overlap is sufficient to create adjacency.

A rectangle that has any point of overlap cannot be considered contained by another rectangle. Therefore, a rectangle does not contain itself either.

Rectangles that are adjacent are assumed not to intersect (as there would be an infinite number of "points" of intersection, again limited by the floating point implementation).

## Testing

Rectangles has a suite of basic unit tests written using the Mocha JavaScript test framework. To run, make sure you've installed Mocha by running `npm install -g mocha` after which you can navigate to the base directory and run `mocha` to run the test suite. The unit tests cover only the rectangles code itself, and not the basic UI.