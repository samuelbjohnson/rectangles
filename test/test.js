//var assert = require('assert')
var should = require('should')
var _ = require('underscore')
var Rectangle = require('../rectangles.js')

var Point = Rectangle.Point

var includePointsFilter = function(correctPoints) {
	return function(value, index) {
		return correctPoints.indexOf(index) >= 0
	}
}

var excludePointsFilter = function(correctPoints) {
	return function(value, index) {
		return correctPoints.indexOf(index) < 0
	}
}

describe('Rectangle', function() {
	var rectangle
	var checkedForSurround
	var checkedForIncidence
	var correctSurroundedPoints = [12]
	var correctIncidentPoints = [6, 7, 8, 11, 13, 16, 17, 18]

	before("Build test rectangle", function() {
		rectangle = new Rectangle(0, 0, 12, 8)
		var points = []
		var xValues = [-1, 0, 6, 12, 13]
		var yValues = [-1, 0, 4, 8, 9]
		xValues.forEach(function(x) {
				yValues.forEach(function(y) {
					points.push(new Point(x, y))
				})
			})
		checkedForSurround = _.map(points, function(p) {
			return rectangle.surroundsPoint(p.x, p.y)
		})
		checkedForIncidence = _.map(points, function(p) {
			return rectangle.isIncident(p.x, p.y)
		})
		
		
	})
	
	describe('surroundsPoint()', function() {
		it('Should return true for surrounded points', function() {
			_.filter(checkedForSurround, includePointsFilter(correctSurroundedPoints)).forEach(function(isSurrounded) {
				(isSurrounded).should.be.exactly(true)
			})
		})
		it('Should return false for incident or not surrounded points', function() {
			_.filter(checkedForSurround, excludePointsFilter(correctSurroundedPoints)).forEach(function(isSurrounded) {
				(isSurrounded).should.be.exactly(false)
			})
		})
		
	})
	
	describe('checkIncidence()', function() {
		it('Should return true for incident points', function() {
			_.filter(checkedForIncidence, includePointsFilter(correctIncidentPoints)).forEach(function(isIncident) {
				(isIncident).should.be.exactly(true)
			})
		})
		
		it('Should return false for non-incident points', function() {
			_.filter(checkedForIncidence, excludePointsFilter(correctIncidentPoints)).forEach(function(isIncident) {
				(isIncident).should.be.exactly(false)
			})
		})
	})
	
	describe('isContainedWithin()', function() {
		it('Should return false with both corners outside', function() {
			(rectangle.isContainedWithin(new Rectangle(1, 1, 10, 6))).should.be.exactly(false)
			;(rectangle.isContainedWithin(new Rectangle(0, 1, 10, 6))).should.be.exactly(false)
		})
		it('Should return false with one corner outside', function() {
			(rectangle.isContainedWithin(new Rectangle(-1, -1, 10, 6))).should.be.exactly(false)
		})
		it('Should return false with one corner inside, one corner incident', function() {
			(rectangle.isContainedWithin(new Rectangle(0, 4, 10, 2))).should.be.exactly(false)
		})
		it('Should return false for identical rectangles', function() {
			(rectangle.isContainedWithin(rectangle)).should.be.exactly(false)
		})
		it('Should return true with both corners inside', function() {
			(rectangle.isContainedWithin(new Rectangle(-1, -1, 14, 10))).should.be.exactly(true)
		})
	})
	
	describe('isAdjacentTo()', function() {
		it('Should return false with both corners outside', function() {
			(rectangle.isAdjacentTo(new Rectangle(1, 1, 10, 6))).should.be.exactly(false)
		})
		it('Should return false with one corner outside', function() {
			(rectangle.isAdjacentTo(new Rectangle(-1, -1, 10, 6))).should.be.exactly(false)
		})
		it('Should return true with one corner inside, one corner incident', function() {
			(rectangle.isAdjacentTo(new Rectangle(0, 4, 10, 2))).should.be.exactly(true)
		})
		it('Should return false for identical rectangles', function() {
			(rectangle.isAdjacentTo(rectangle)).should.be.exactly(false)
		})
		it('Should return false with both corners inside', function() {
			(rectangle.isAdjacentTo(new Rectangle(-1, -1, 14, 10))).should.be.exactly(false)
		})
		it('Should return true with both corners outside, 1 dimension incident', function() {
			(rectangle.isAdjacentTo(new Rectangle(0, 1, 10, 6))).should.be.exactly(true)
		})
	})

	describe('findIntersections()', function() {
		it('Should return [] with both corners outside', function() {
			var intersections = rectangle.findIntersections(new Rectangle(1, 1, 10, 6))
			;(intersections).should.be.an.Array()
			;(intersections).should.be.empty()
		})
		it('Should return intersections with one corner outside: one vertical, one horizontal intersection', function() {
			var intersections = rectangle.findIntersections(new Rectangle(-1, -1, 10, 6))
			;(intersections).should.be.an.Array()
			;(intersections).should.have.length(2)
			;(new Point(9, 0).existsIn(intersections)).should.be.exactly(true)
			;(new Point(0, 5).existsIn(intersections)).should.be.exactly(true)
		})
		it('Should return intersections with one corner outside: two vertical intersections', function() {
			var intersections = rectangle.findIntersections(new Rectangle(10, 2, 5, 5))
			;(intersections).should.be.an.Array()
			;(intersections).should.have.length(2)
			;(new Point(12, 2).existsIn(intersections)).should.be.exactly(true)
			;(new Point(12, 7).existsIn(intersections)).should.be.exactly(true)
		})
		it('Should return intersections with one corner outside: two horizontal intersections', function() {
			var intersections = rectangle.findIntersections(new Rectangle(1, 6, 5, 5))
			;(intersections).should.be.an.Array()
			;(intersections).should.have.length(2)
			;(new Point(1, 8).existsIn(intersections)).should.be.exactly(true)
			;(new Point(6, 8).existsIn(intersections)).should.be.exactly(true)
		})
		it('Should return [] with one corner inside, one corner incident', function() {
			var intersections = rectangle.findIntersections(new Rectangle(0, 4, 10, 2))
			;(intersections).should.be.an.Array()
			;(intersections).should.be.empty()
		})
		it('Should return [] for identical rectangles', function() {
			var intersections = rectangle.findIntersections(rectangle)
			;(intersections).should.be.an.Array()
			;(intersections).should.be.empty()
		})
		it('Should return [] with both corners inside', function() {
			var intersections = rectangle.findIntersections(new Rectangle(-1, -1, 14, 10))
			;(intersections).should.be.an.Array()
			;(intersections).should.be.empty()
		})
		it('Should return [] with both corners outside, 1 dimension incident', function() {
			var intersections = rectangle.findIntersections(new Rectangle(0, 1, 10, 6))
			;(intersections).should.be.an.Array()
			;(intersections).should.be.empty()
		})
	})
})